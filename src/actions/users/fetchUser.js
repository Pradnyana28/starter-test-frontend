import axios from "axios"
import { FETCH_USER } from '../types'

const fetchUser = () => async dispatch => {
    let data = []
    const existedUsers = JSON.parse(localStorage.getItem('users'))
    if (existedUsers) {
        data = existedUsers
        dispatch({ type: FETCH_USER, users: existedUsers })
    } else {
        const users = await axios.get("https://jsonplaceholder.typicode.com/users")
        if ( users ) {
            data = users.data
            localStorage.setItem('users', JSON.stringify(data))
            dispatch({ type: FETCH_USER, users: data })
        }
    }
    return data;
}

export default fetchUser