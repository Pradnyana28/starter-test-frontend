import React from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';

import Home from './containers/Home/Home';
import Pricing from './containers/Pricing/Pricing';
import Checkout from './containers/Checkout/Checkout';
import Users from './containers/Users/Users';

function App() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/pricing" exact component={Pricing} />
      <Route path="/checkout" exact component={Checkout} />
      <Route path="/users" exact component={Users} />
      <Redirect to="/" />
    </Switch>
  );
}

export default App;
