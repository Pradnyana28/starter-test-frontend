import React, { memo } from 'react';
import { Main, Container, Field, Row, Footer, Header, CartSideBar } from '../../components';

const CARTS = [
  { title: "Product title", description: "Briefly description", price: 25 },
  { title: "Product title", description: "Briefly description", price: 25 },
  { title: "Product title", description: "Briefly description", price: 25, highlight: true }
]

const Checkout = memo(() => {
  return (
    <>
      <Header />
      <Main>
        <Container>
        <div className="py-5 text-center">
          <img className="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
          <h2>Checkout form</h2>
          <p className="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
        </div>

        <Row>
          <div className="col-md-4 order-md-2 mb-4">
            <CartSideBar carts={CARTS} />
          </div>
          <div className="col-md-8 order-md-1">
            <h4 className="mb-3">Billing address</h4>
            <form className="needs-validation" noValidate="">
              <Row>
                <div className="col-md-6 mb-3">
                  <Field 
                    label="First Name"
                    name="firstName"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
                <div className="col-md-6 mb-3">
                  <Field 
                    label="Last Name"
                    name="lastName"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
              </Row>

              <div className="mb-3">
                <Field 
                  label="Username"
                  name="username"
                  type="text"
                  className="form-control"
                  placeholder="Username"
                  isGroup
                  prepend="@"
                  required
                />
              </div>

              <div className="mb-3">
                <Field 
                  label="Email"
                  name="email"
                  type="email"
                  className="form-control"
                  isOptional
                />
              </div>

              <div className="mb-3">
                <Field 
                  label="Address"
                  name="address"
                  type="text"
                  className="form-control"
                  required
                />
              </div>

              <div className="mb-3">
                <Field 
                  label="Address 2"
                  name="address2"
                  type="text"
                  className="form-control"
                  isOptional
                />
              </div>

              <div className="row">
                <div className="col-md-5 mb-3">
                  <Field 
                    label="Country"
                    className="custom-select d-block w-100"
                    type="select"
                    options={[
                      { value: '', text: 'Choose...' },
                      { value: 'us', text: 'United States' },
                      { value: 'uk', text: 'United Kingdom' }
                    ]}
                  />
                </div>
                <div className="col-md-4 mb-3">
                  <Field 
                    label="State"
                    className="custom-select d-block w-100"
                    type="select"
                    options={[
                      { value: '', text: 'Choose...' },
                      { value: 'california', text: 'California' },
                      { value: 'new-york', text: 'New York' }
                    ]}
                  />
                </div>
                <div className="col-md-3 mb-3">
                  <Field 
                    label="Zip"
                    name="zip"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
              </div>
              <hr className="mb-4" />
              <div className="custom-control custom-checkbox">
                <Field 
                  id="same-address"
                  label="Shipping address is the same as my billing address"
                  type="checkbox"
                  name="same-address"
                  className="custom-control-input"
                  labelClass="custom-control-label"
                />
              </div>
              <div className="custom-control custom-checkbox">
                <Field 
                  id="save-info"
                  label="Save this information for next time"
                  type="checkbox"
                  name="save-info"
                  className="custom-control-input"
                  labelClass="custom-control-label"
                />
              </div>
              <hr className="mb-4" />

              <h4 className="mb-3">Payment</h4>

              <div className="d-block my-3">
                <div className="custom-control custom-radio">
                  <Field 
                    id="credit"
                    label="Credit card"
                    type="radio"
                    name="paymentMethod"
                    className="custom-control-input"
                    labelClass="custom-control-label"
                  />
                </div>
                <div className="custom-control custom-radio">
                  <Field 
                    id="debit"
                    label="Debit card"
                    type="radio"
                    name="paymentMethod"
                    className="custom-control-input"
                    labelClass="custom-control-label"
                  />
                </div>
                <div className="custom-control custom-radio">
                  <Field 
                    id="paypal"
                    label="PayPal"
                    type="radio"
                    name="paymentMethod"
                    className="custom-control-input"
                    labelClass="custom-control-label"
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-6 mb-3">
                  <Field 
                    label="Name on card"
                    description="Full name as displayed on card"
                    name="cc-name"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
                <div className="col-md-6 mb-3">
                  <Field 
                    label="Credit card number"
                    name="cc-number"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-3 mb-3">
                  <Field 
                    label="Expiration"
                    name="cc-expiration"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
                <div className="col-md-3 mb-3">
                  <Field 
                    label="CVV"
                    name="cc-cvv"
                    type="text"
                    className="form-control"
                    required
                  />
                </div>
              </div>
              <hr className="mb-4" />
              <button className="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
            </form>
          </div>
        </Row>

        <Footer />
      </Container>
      </Main>
    </>
  );
})

export default Checkout;
