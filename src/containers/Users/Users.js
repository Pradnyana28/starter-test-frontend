import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import { Header, Main, Container, Row, Card, Footer } from '../../components'

class Users extends Component {
    constructor(props) {
        super(props)

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        this.getUsers()
    }

    getUsers = async () => {
        const users = await this.props.fetchUser()
        if ( users.length > 0 ) {
            this.setState({ users })
        }
    }

    renderUsers = () => {
        const { users } = this.state
        if ( users.length > 0 ) {
            return users.map((user, idx) => (
                <div key={idx} className="col-md-4">
                    <Card description={(
                        <>
                            <span className="d-block">{user.name}</span>
                            <span className="d-block">{user.email}</span>
                        </>
                    )} />
                </div>
            ))
        }
    }

    render() {
        return (
        <>
            <Header />
            <Main>
                <Container>
                    <Row>
                        {this.renderUsers()}
                    </Row>
                </Container>
            </Main>
            <Footer />
        </>
        )
    }
}

export default connect(null, actions)(Users)