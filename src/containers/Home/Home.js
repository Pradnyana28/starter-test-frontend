import React, { memo } from 'react';
import { Main, Row, Header, Card, Jumbotron, Footer, Container } from '../../components'

const DATA_CARD = [
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" },
  { description: "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.", textMuted: "9 mins" }
]

const Home = memo(() => {

  const renderCards = () => DATA_CARD.map((data, idx) => (
    <div key={idx} className="col-md-4">
      <Card description={data.description} textMuted={data.textMuted} />
    </div>
  ))

  return (
    <>
      <Header />
      <Main>
        <Jumbotron />
        <div className="album py-5 bg-light">
          <Container>
            <Row>
              {renderCards()}
            </Row>
          </Container>
        </div>
      </Main>
      <Footer />
    </>
  );
})

export default Home;
