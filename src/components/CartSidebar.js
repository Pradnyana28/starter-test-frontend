import React, { memo, useState } from 'react'
import { GroupList, ListItem } from '../components'

const CartSidebar = memo(({ carts }) => {
  let [total] = useState(0)
  if ( carts.length > 0 ) {
    carts.forEach(cart => {
      total = total + parseFloat(cart.price)
    })
  }

  const renderCartList = () => {
    if ( carts.length > 0 ) {
      return (
        <GroupList>
            {carts.map((cart, idx) => <ListItem key={idx} title={cart.title} description={cart.description} right={`$${cart.price}`} isHighlight={cart.highlight} />)}
            <li className="list-group-item d-flex justify-content-between">
              <span>Total (USD)</span>
              <strong>${total}</strong>
            </li>
        </GroupList>
      )
    }

    return <p>Your cart is empty.</p>
  }

    return (
        <>
            <h4 className="d-flex justify-content-between align-items-center mb-3">
              <span className="text-muted">Your cart</span>
              <span className="badge badge-secondary badge-pill">3</span>
            </h4>
            {renderCartList()}

            <form className="card p-2">
              <div className="input-group">
                <input type="text" className="form-control" placeholder="Promo code" />
                <div className="input-group-append">
                  <button type="submit" className="btn btn-secondary">Redeem</button>
                </div>
              </div>
            </form>
        </>
    )
})

CartSidebar.defaultProps = {
  carts: []
}

export default CartSidebar