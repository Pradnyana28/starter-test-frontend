import React, { memo } from 'react'

const GroupList = memo(({ children }) => {
    return <ul className="list-group mb-3">{children}</ul>
})

export default GroupList