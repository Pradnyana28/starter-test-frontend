import React, { memo } from 'react'

const Input = memo(({ name, ...rest }) => {
    const hasDescription = () => {
        if (rest.description !== undefined) {
            return <small className="text-muted">{rest.description}</small>
        }
    }

    return (
        <>
            <input name={name} {...rest} />
            {hasDescription()}
        </>
    )
})

export default Input