import React, { memo } from 'react'

const Footer = memo(() => {
    return (
        <footer className="text-muted">
            <div className="container">
            <p className="float-right">
                <button data-target="top">Back to top</button>
            </p>
            <p>Album example is © Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap? <a href="https://getbootstrap.com/">Visit the homepage</a> or read our <a href="/docs/4.3/getting-started/introduction/">getting started guide</a>.</p>
            </div>
        </footer>
    )
})

export default Footer