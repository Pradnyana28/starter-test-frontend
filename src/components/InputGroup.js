import React, { memo } from 'react'

export default memo(({ children }) => {
    return <div className="input-group">{children}</div>
})