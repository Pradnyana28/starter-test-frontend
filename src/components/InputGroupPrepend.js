import React, { memo } from 'react'

export default memo(({ text }) => {
    return (
        <div className="input-group-prepend">
            <span className="input-group-text">{text}</span>
        </div>
    )
})