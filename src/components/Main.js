import React, { memo } from 'react'

const Main = memo(({ children }) => {
    return <main role="main">{children}</main>
})

export default Main