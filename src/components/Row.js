import React, { memo } from 'react'

const Row = memo(({ children }) => {
    return <div className="row">{children}</div>
})

export default Row