import React, { memo } from 'react'
import { Input, InputGroup, InputGroupPrepend } from '.';

const Field = memo(({ label, isOptional, isGroup, labelClass, ...rest }) => {
    const showOptional = () => {
        if (isOptional) {
            return <span className="text-muted">(Optional)</span>
        }
    }

    const renderInput = () => {
        let SelectedField = () => <Input {...rest} />
        if ( rest.type === 'select' ) {
            SelectedField = () => (
                <select {...rest}>
                    {rest.options.map((opt, idx) => <option key={idx} value={opt.value}>{opt.text}</option>)}
                </select>
            )
        }

        if (isGroup) {
            return (
                <InputGroup>
                    <InputGroupPrepend text={rest.prepend} />
                    <SelectedField />
                </InputGroup>
            )
        }

        return <SelectedField />
    }

    const renderLabelBefore = () => {
        if ( 
            rest.type !== 'checkbox' && 
            rest.type !== 'radio' 
        ) {
            return <label htmlFor={rest.id} className={labelClass}>{label} {showOptional()}</label>
        }
    }

    const renderLabelAfter = () => {
        if ( 
            rest.type === 'checkbox' || 
            rest.type === 'radio' 
        ) {
            return <label htmlFor={rest.id} className={labelClass}>{label} {showOptional()}</label>
        }
    }

    return (
        <>
            {renderLabelBefore()}
            {renderInput()}
            {renderLabelAfter()}
            <div className="invalid-feedback">{rest.error}</div>
        </>
    )
})

Field.defaultProps = {
    isOptional: false,
    isGroup: false
}

export default Field