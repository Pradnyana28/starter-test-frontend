import React, { memo } from 'react'

const ListItem = memo(({ title, description, right, isHighlight }) => {
    const wrapperHighlight = isHighlight ? 'bg-light' : 'lh-condensed'
    const bgHighlight = isHighlight ? 'text-success' : ''
    const textHighlight = isHighlight ? 'text-success' : 'text-muted'

    return (
        <li className={`list-group-item d-flex justify-content-between ${wrapperHighlight}`}>
            <div className={bgHighlight}>
                <h6 className="my-0">{title}</h6>
                <small className={bgHighlight}>{description}</small>
            </div>
            <span className={textHighlight}>{right}</span>
        </li>
    )
})

export default ListItem