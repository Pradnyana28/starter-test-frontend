import React, { memo } from 'react'

const Container = memo(({ children }) => {
    return <div className="container">{children}</div>
})

export default Container